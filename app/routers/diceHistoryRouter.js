const express = require("express");

const {
  createDiceHistory,
  updateDiceHistory,
  getDiceHistory,
  getDiceHistorys,
  deleteDiceHistory,
} = require("../controllers/diceHistoryController");

// Define Router
const diceHistoryRouter = express.Router();
diceHistoryRouter.route("/dice-histories").post(createDiceHistory).get(getDiceHistorys);

diceHistoryRouter.route("/dice-histories/:id").delete(deleteDiceHistory).patch(updateDiceHistory).get(getDiceHistory);

module.exports = { diceHistoryRouter };
