const express = require("express");

const {
  createVoucherHistory,
  updateVoucherHistory,
  getVoucherHistory,
  getVoucherHistorys,
  deleteVoucherHistory,
} = require("../controllers/voucherHistoryController");

// Define Router
const voucherHistoryRouter = express.Router();
voucherHistoryRouter.route("/voucher-histories").post(createVoucherHistory).get(getVoucherHistorys);

voucherHistoryRouter
  .route("/voucher-histories/:id")
  .delete(deleteVoucherHistory)
  .patch(updateVoucherHistory)
  .get(getVoucherHistory);

module.exports = { voucherHistoryRouter };
