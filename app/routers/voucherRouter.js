const express = require("express");

const {
  createVoucher,
  updateVoucher,
  getVoucher,
  getVouchers,
  deleteVoucher,
} = require("../controllers/voucherController");

// Define Router
const voucherRouter = express.Router();
voucherRouter.route("/vouchers").post(createVoucher).get(getVouchers);

voucherRouter.route("/vouchers/:id").delete(deleteVoucher).patch(updateVoucher).get(getVoucher);

module.exports = { voucherRouter };
