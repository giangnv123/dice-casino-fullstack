const express = require("express");

const { createPrize, updatePrize, getPrize, getPrizes, deletePrize } = require("../controllers/prizeController");

// Define Router
const prizeRouter = express.Router();
prizeRouter.route("/prizes").post(createPrize).get(getPrizes);

prizeRouter.route("/prizes/:id").delete(deletePrize).patch(updatePrize).get(getPrize);

module.exports = { prizeRouter };
