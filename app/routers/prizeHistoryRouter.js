const express = require("express");

const {
  createPrizeHistory,
  updatePrizeHistory,
  getPrizeHistory,
  getPrizeHistorys,
  deletePrizeHistory,
} = require("../controllers/prizeHistoryController");

// Define Router
const prizeHistoryRouter = express.Router();
prizeHistoryRouter.route("/prize-histories").post(createPrizeHistory).get(getPrizeHistorys);

prizeHistoryRouter
  .route("/prize-histories/:id")
  .delete(deletePrizeHistory)
  .patch(updatePrizeHistory)
  .get(getPrizeHistory);

module.exports = { prizeHistoryRouter };
