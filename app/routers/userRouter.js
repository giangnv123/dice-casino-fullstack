const express = require("express");

const { createUser, updateUser, getUser, getUsers, deleteUser } = require("../controllers/userController");

// Define Router
const userRouter = express.Router();
userRouter.route("/users").post(createUser).get(getUsers);

userRouter.route("/users/:id").delete(deleteUser).patch(updateUser).get(getUser);

module.exports = { userRouter };
