const express = require("express");

// Get middleware
const diceMiddleware = require("../middleware/diceMiddleware");

const { getRandomNumber, playDice } = require("../controllers/diceController");

/***** ROUTER *****/

// Create router
const diceRouter = express.Router();

diceRouter.route("/random-number").get(diceMiddleware.getCurrentTimeMiddleWare, getRandomNumber);
diceRouter.route("/dice").post(diceMiddleware.validatePlayDiceMiddleWear, playDice);

// Export Course Router
module.exports = { diceRouter };
