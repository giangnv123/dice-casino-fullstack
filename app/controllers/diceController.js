const User = require("../models/userModel");
const DiceHistory = require("../models/diceHistoryModel");
const Voucher = require("../models/voucherModel");
const Prize = require("../models/prizeModel");
const PrizeHistory = require("../models/prizeHistoryModel");
const VoucherHistory = require("../models/voucherHistoryModel");

exports.getRandomNumber = (req, res) => {
  const number = Math.trunc(Math.random() * 6) + 1;
  res.status(200).json({
    messsage: "Success",
    number,
  });
};

exports.playDice = async (req, res) => {
  try {
    const fieldsOut = "-__v -createdAt -updatedAt";

    let randomDice = Math.floor(Math.random() * 6) + 1;

    let voucher = null;
    let prize = null;
    let getPrize = true;

    // 1) Check existing User
    let user = await User.findOne({
      username: req.body.username,
    });

    if (!user) user = await User.create(req.body);

    // 2) Create new Dicehistory
    const diceHistoryObj = {
      user: user.id,
      dice: randomDice,
    };
    await DiceHistory.create(diceHistoryObj);

    // 3) Check and Get Voucher
    if (randomDice > 3) {
      // Get random Voucher
      const voucherQuantity = await Voucher.countDocuments();
      const randomVoucherNo = Math.floor(Math.random() * voucherQuantity);
      voucher = await Voucher.findOne().skip(randomVoucherNo).select(fieldsOut);

      // Create new Voucher History
      await VoucherHistory.create({
        voucher: voucher.id,
        user: user.id,
      });
    }

    // 4) Check Three lastest dice of user
    let diceHistorys = await DiceHistory.find({ user: user.id }).sort("-createdAt").limit(3);

    getPrize = diceHistorys.length < 3 ? false : diceHistorys.filter((el) => el.dice <= 3).length === 0;

    // 5) Check and Get Prize
    if (getPrize === true) {
      // Get random prize
      const allPizeQuantity = await Prize.countDocuments();
      const randomPrizeNo = Math.floor(Math.random() * allPizeQuantity);
      prize = await Prize.findOne().skip(randomPrizeNo).select(fieldsOut);

      // Create new Prize History
      await PrizeHistory.create({
        user: user.id,
        prize: prize.id,
      });
    }

    // 6) Perform Response
    res.status(200).json({
      dice: randomDice,
      diceHistory: diceHistoryObj,
      voucher,
      prize,
    });
  } catch (error) {
    res.status(400).json({
      message: "Fail!",
      error,
    });
  }
};
