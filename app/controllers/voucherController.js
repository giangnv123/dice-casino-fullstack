const Voucher = require("../models/voucherModel");

// Get All Vouchers
exports.getVouchers = async (req, res) => {
  try {
    const vouchers = await Voucher.find();

    res.status(200).json({
      message: "Success",
      result: vouchers.length,
      vouchers,
    });
  } catch (error) {
    res.status(400).json({
      message: "Fail to get all vouchers!",
      error,
    });
  }
};

// Get Voucher
exports.getVoucher = async (req, res) => {
  try {
    const voucher = await Voucher.findById(req.params.id);
    res.status(200).json({
      message: "Success",
      voucher,
    });
  } catch (error) {
    res.status(400).json({
      message: "Fail to get voucher!",
      error,
    });
  }
};

// Update Voucher
exports.updateVoucher = async (req, res) => {
  try {
    const voucher = await Voucher.findByIdAndUpdate(req.params.id, req.body);
    res.status(200).json({
      message: "Success",
      voucher,
    });
  } catch (error) {
    res.status(400).json({
      message: "Fail to update voucher.",
      error,
    });
  }
};

// Create Voucher
exports.createVoucher = async (req, res) => {
  try {
    const voucher = await Voucher.create(req.body);
    res.status(201).json({
      message: "Success",
      voucher,
    });
  } catch (error) {
    res.status(400).json({
      message: "Fail to create new Dice History!",
      error,
    });
  }
};

// Delete Voucher

exports.deleteVoucher = async (req, res) => {
  try {
    const voucher = await Voucher.findByIdAndDelete(req.params.id);
    res.status(204).json({
      message: "Success",
      voucher,
    });
  } catch (error) {
    res.status(400).json({
      message: "Fail to delete Voucher!",
      error,
    });
  }
};
