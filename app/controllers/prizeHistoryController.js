const PrizeHistory = require("../models/prizeHistoryModel");

// Get All PrizeHistorys
exports.getPrizeHistorys = async (req, res) => {
  let condition = {};
  if (req.query.user) {
    condition.user = req.query.user;
  }
  try {
    const prizeHistorys = await PrizeHistory.find(condition);

    if (!prizeHistorys.length)
      return res.status(404).json({
        message: "No Prize History Found!",
      });

    res.status(200).json({
      message: "Success",
      result: prizeHistorys.length,
      prizeHistorys,
    });
  } catch (error) {
    res.status(400).json({
      message: "Fail to get all prize Historys!",
      error,
    });
  }
};

// Get PrizeHistory
exports.getPrizeHistory = async (req, res) => {
  try {
    const prizeHistory = await PrizeHistory.findById(req.params.id);
    res.status(200).json({
      message: "Success",
      prizeHistory,
    });
  } catch (error) {
    res.status(400).json({
      message: "Fail to get prize History!",
      error,
    });
  }
};

// Update PrizeHistory
exports.updatePrizeHistory = async (req, res) => {
  try {
    const prizeHistory = await PrizeHistory.findByIdAndUpdate(req.params.id, req.body);
    res.status(200).json({
      message: "Success",
      prizeHistory,
    });
  } catch (error) {
    res.status(400).json({
      message: "Fail to update prize History.",
      error,
    });
  }
};

// Create PrizeHistory
exports.createPrizeHistory = async (req, res) => {
  try {
    const prizeHistory = await PrizeHistory.create(req.body);
    res.status(201).json({
      message: "Success",
      prizeHistory,
    });
  } catch (error) {
    res.status(400).json({
      message: "Fail to create new Prize History!",
      error,
    });
  }
};

// Delete PrizeHistory

exports.deletePrizeHistory = async (req, res) => {
  try {
    const prizeHistory = await PrizeHistory.findByIdAndDelete(req.params.id);
    res.status(204).json({
      message: "Success",
      prizeHistory,
    });
  } catch (error) {
    res.status(400).json({
      message: "Fail to delete Prize History!",
      error,
    });
  }
};
