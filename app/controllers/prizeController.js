const Prize = require("../models/prizeModel");

// Get All Prizes
exports.getPrizes = async (req, res) => {
  try {
    const prizes = await Prize.find();
    res.status(200).json({
      message: "Success",
      result: prizes.length,
      prizes,
    });
  } catch (error) {
    res.status(400).json({
      message: "Fail to get all prizes!",
      error,
    });
  }
};

// Get Prize
exports.getPrize = async (req, res) => {
  try {
    const prize = await Prize.findById(req.params.id);
    res.status(200).json({
      message: "Success",
      prize,
    });
  } catch (error) {
    res.status(400).json({
      message: "Fail to get prize!",
      error,
    });
  }
};

// Update Prize
exports.updatePrize = async (req, res) => {
  try {
    const prize = await Prize.findByIdAndUpdate(req.params.id, req.body);
    res.status(200).json({
      message: "Success",
      prize,
    });
  } catch (error) {
    res.status(400).json({
      message: "Fail to update prize.",
      error,
    });
  }
};

// Create Prize
exports.createPrize = async (req, res) => {
  try {
    const prize = await Prize.create(req.body);
    res.status(201).json({
      message: "Success",
      prize,
    });
  } catch (error) {
    res.status(400).json({
      message: "Fail to create new Dice History!",
      error,
    });
  }
};

// Delete Prize

exports.deletePrize = async (req, res) => {
  try {
    const prize = await Prize.findByIdAndDelete(req.params.id);
    res.status(204).json({
      message: "Success",
      prize,
    });
  } catch (error) {
    res.status(400).json({
      message: "Fail to delete Prize!",
      error,
    });
  }
};
