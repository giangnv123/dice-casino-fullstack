const mongoose = require("mongoose");
const DiceHistory = require("../models/diceHistoryModel");

// Get All DiceHistorys
exports.getDiceHistorys = async (req, res) => {
  try {
    let condition = {};
    const userId = req.query.userId;
    if (userId) condition.user = userId;

    const diceHistorys = await DiceHistory.find(condition);
    res.status(200).json({
      message: "Success",
      results: diceHistorys.length,
      diceHistorys,
    });
  } catch (error) {
    res.status(400).json({
      message: "Fail to get all dice Histories!",
      error,
    });
  }
};

// Get DiceHistory
exports.getDiceHistory = async (req, res) => {
  try {
    const id = req.params.id;
    if (!mongoose.Types.ObjectId.isValid(id)) {
      return res.status(404).json({
        message: "Invalid Id Type!",
      });
    }

    const diceHistory = await DiceHistory.findByIdAndUpdate(req.params.id, req.body, {
      new: true,
      runValidators: true,
    });

    if (!diceHistory)
      return res.status(404).json({
        message: "Dice History not found!",
      });

    res.status(200).json({
      message: "Success",
      diceHistory,
    });
  } catch (error) {
    res.status(400).json({
      message: "Fail to get dice History!",
      error,
    });
  }
};

// Update DiceHistory
exports.updateDiceHistory = async (req, res) => {
  try {
    const id = req.params.id;
    if (!mongoose.Types.ObjectId.isValid(id)) {
      return res.status(404).json({
        message: "Invalid Id Type!",
      });
    }

    const diceHistory = await DiceHistory.findByIdAndUpdate(req.params.id, req.body, {
      new: true,
      runValidators: true,
    });

    if (!diceHistory)
      return res.status(404).json({
        message: "Dice History not found!",
      });

    res.status(200).json({
      message: "Success",
      diceHistory,
    });
  } catch (error) {
    res.status(400).json({
      message: "Fail to update diceHistory.",
      error,
    });
  }
};

// Create DiceHistory
exports.createDiceHistory = async (req, res) => {
  const randomNumber = Math.trunc(Math.random() * 6) + 1;
  if (req.body.dice)
    return res.status(400).json({
      message: "Please don't provide dice number",
    });
  try {
    const diceHistory = await DiceHistory.create({ ...req.body, dice: randomNumber });

    res.status(201).json({
      message: "Success",
      diceHistory,
    });
  } catch (error) {
    res.status(400).json({
      message: "Fail to create Dice History!",
      error,
    });
  }
};

// Delete DiceHistory

exports.deleteDiceHistory = async (req, res) => {
  try {
    const id = req.params.id;

    if (!mongoose.Types.ObjectId.isValid(id)) {
      return res.status(404).json({
        message: "Invalid Id Type!",
      });
    }

    const diceHistory = await DiceHistory.findByIdAndDelete(id);

    if (!diceHistory)
      return res.status(404).json({
        message: "Dice History not found!",
      });

    res.status(204).json({
      message: "Success",
      diceHistory,
    });
  } catch (error) {
    res.status(400).json({
      message: "Fail to delete DiceHistory!",
      error,
    });
  }
};
