const VoucherHistory = require("../models/voucherHistoryModel");

// Get All VoucherHistorys
exports.getVoucherHistorys = async (req, res) => {
  let condition = {};
  if (req.query.user) {
    condition.user = req.query.user;
  }
  try {
    const voucherHistorys = await VoucherHistory.find(condition);
    res.status(200).json({
      message: "Success",
      results: voucherHistorys.length,
      voucherHistorys,
    });
  } catch (error) {
    res.status(400).json({
      message: "Fail to get all voucher Historys!",
      error,
    });
  }
};

// Get VoucherHistory
exports.getVoucherHistory = async (req, res) => {
  try {
    const voucherHistory = await VoucherHistory.findById(req.params.id);
    res.status(200).json({
      message: "Success",
      voucherHistory,
    });
  } catch (error) {
    res.status(400).json({
      message: "Fail to get voucherHistory!",
      error,
    });
  }
};

// Update VoucherHistory
exports.updateVoucherHistory = async (req, res) => {
  try {
    const voucherHistory = await VoucherHistory.findByIdAndUpdate(req.params.id, req.body);
    res.status(200).json({
      message: "Success",
      voucherHistory,
    });
  } catch (error) {
    res.status(400).json({
      message: "Fail to update voucherHistory.",
      error,
    });
  }
};

// Create VoucherHistory
exports.createVoucherHistory = async (req, res) => {
  try {
    const voucherHistory = await VoucherHistory.create(req.body);
    res.status(201).json({
      message: "Success",
      voucherHistory,
    });
  } catch (error) {
    res.status(400).json({
      message: "Fail to create new Dice History!",
      error,
    });
  }
};

// Delete VoucherHistory

exports.deleteVoucherHistory = async (req, res) => {
  try {
    const voucherHistory = await VoucherHistory.findByIdAndDelete(req.params.id);
    res.status(204).json({
      message: "Success",
      voucherHistory,
    });
  } catch (error) {
    res.status(400).json({
      message: "Fail to delete VoucherHistory!",
      error,
    });
  }
};
