const User = require("../models/userModel");

// Get All Users
exports.getUsers = async (req, res) => {
  let condition = {};
  if (req.query.user) {
    condition._id = req.query.user;
  }

  try {
    const users = await User.find(condition);
    res.status(200).json({
      message: "Success",
      results: users.length,
      users,
    });
  } catch (error) {
    res.status(400).json({
      message: "Fail to get all users!",
      error,
    });
  }
};

// Get User
exports.getUser = async (req, res) => {
  try {
    const user = await User.findById(req.params.id);
    res.status(200).json({
      message: "Success",
      user,
    });
  } catch (error) {
    res.status(400).json({
      message: "Fail to get user!",
      error,
    });
  }
};

// Update User
exports.updateUser = async (req, res) => {
  try {
    console.log("find user");
    const user = await User.findByIdAndUpdate(req.params.id, req.body, {
      new: true,
      runValidators: true,
    });
    console.log(user, "user");
    if (!user)
      return res.status(404).json({
        message: "User not found!",
      });

    res.status(200).json({
      message: "Success",
      user,
    });
  } catch (error) {
    res.status(400).json({
      message: "Unsupported User's id type!",
      error,
    });
  }
};

// Create User
exports.createUser = async (req, res) => {
  try {
    const user = await User.create(req.body);
    res.status(201).json({
      message: "Success",
      user,
    });
  } catch (error) {
    res.status(400).json({
      message: "Fail to create new Dice History!",
      error,
    });
  }
};

// Delete User

exports.deleteUser = async (req, res) => {
  try {
    const user = await User.findByIdAndDelete(req.params.id);
    res.status(204).json({
      message: "Success",
      user,
    });
  } catch (error) {
    res.status(400).json({
      message: "Fail to delete User!",
      error,
    });
  }
};
