const mongoose = require("mongoose");

const VoucherHistorySchema = new mongoose.Schema({
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
    required: [true, "Please provide user's id"],
  },
  voucher: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Voucher",
    required: [true, "Please provide voucher id"],
  },
  createdAt: {
    type: Date,
    default: Date.now(),
  },
  updatedAt: {
    type: Date,
    default: Date.now(),
  },
});

const VoucherHistory = mongoose.model("VoucherHistory", VoucherHistorySchema);

module.exports = VoucherHistory;
