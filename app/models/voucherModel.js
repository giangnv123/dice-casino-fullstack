const mongoose = require("mongoose");

const voucherSchema = new mongoose.Schema({
  code: {
    type: String,
    unique: true,
    required: [true, "Please provide a code for the voucher"],
    trim: [true],
  },
  discount: {
    type: Number,
    required: [true, "Please provide a discount for the voucher"],
    trim: [true],
  },
  note: {
    type: String,
    trim: [true],
  },
  createdAt: {
    type: Date,
    default: Date.now(),
  },
  updatedAt: {
    type: Date,
    default: Date.now(),
  },
});

const Voucher = mongoose.model("Voucher", voucherSchema);

module.exports = Voucher;
