const mongoose = require("mongoose");

const PrizeHistorySchema = new mongoose.Schema({
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
    required: true,
  },
  prize: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Prize",
    required: true,
    trim: [true],
  },
  createdAt: {
    type: Date,
    default: Date.now(),
  },
  updatedAt: {
    type: Date,
    default: Date.now(),
  },
});

const PrizeHistory = mongoose.model("PrizeHistory", PrizeHistorySchema);

module.exports = PrizeHistory;
