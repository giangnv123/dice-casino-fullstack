const mongoose = require("mongoose");

const prizeSchema = new mongoose.Schema({
  name: {
    type: String,
    unique: true,
    required: [true, "Please provide a name for the prize"],
    trim: true,
  },
  description: {
    type: String,
    trim: [true],
  },
  createdAt: {
    type: Date,
    default: Date.now(),
    select: false,
  },
  updatedAt: {
    type: Date,
    default: Date.now(),
    select: false,
  },
});

const Prize = mongoose.model("Prize", prizeSchema);

module.exports = Prize;
