const mongoose = require("mongoose");

const diceHistorySchema = new mongoose.Schema({
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
    required: [true, "Please provide User's id!"],
  },
  dice: {
    type: Number,
    required: [true, "Please provide dice number!"],
  },
  createdAt: {
    type: Date,
    default: Date.now(),
    select: false,
  },
  updatedAt: {
    type: Date,
    default: Date.now(),
    select: false,
  },
});

const DiceHistory = mongoose.model("DiceHistory", diceHistorySchema);

module.exports = DiceHistory;
