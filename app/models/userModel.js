const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
  username: {
    type: String,
    unique: true,
    required: [true, "User must have a username."],
    trim: [true],
  },
  firstname: {
    type: String,
    required: [true, "User must have a firstname."],
    trim: [true],
  },
  lastname: {
    type: String,
    required: [true, "User must have a lastname."],
    trim: [true],
  },
  createdAt: {
    type: Date,
    default: Date.now(),
  },
  updatedAt: {
    type: Date,
    default: Date.now(),
  },
});

const User = mongoose.model("User", userSchema);

module.exports = User;
