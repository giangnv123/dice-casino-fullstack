exports.getCurrentTimeMiddleWare = (req, res, next) => {
  const currentTime = new Date(Date.now()).toLocaleTimeString();
  console.log(`Current Time is: ${currentTime}`);
  console.log(`The request method is: ${req.method}`);
  next();
};

exports.validatePlayDiceMiddleWear = async (req, res, next) => {
  const { username, firstname, lastname } = req.body;
  console.log("Username: " + username);
  if (!username || !firstname || !lastname)
    return res.status(400).json({
      status: "Fail",
      message: "Please Provide All username, firstname and lastname!",
    });
  next();
};
