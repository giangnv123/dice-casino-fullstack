"use strict";

const urlBase = `devcamp-lucky-dice/dice`;

async function performPostMethod() {
  let userObj = {
    firstname: "Giang",
    lastname: "Nguyen",
    username: "HongAnhaaaaa",
  };
  try {
    const response = await fetch(urlBase, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(userObj),
    });

    if (!response.ok) {
      throw new Error("Request failed");
    }

    const result = await response.json();
    return result;
  } catch (error) {
    console.error("Error:", error);
    throw error;
  }
}

async function rollDice() {
  const diceResult = await performPostMethod();
  console.log(diceResult);
  updateUserInterface(diceResult.dice);
}

function updateUserInterface(diceNumber) {
  $("#img-dice").attr("src", `images/${diceNumber}.png`);

  const message = diceNumber < 4 ? "Thank you! Better Luck next time" : "Congratulation! Play Again!";
  const color = diceNumber < 4 ? "purple" : "orange";

  $("#feedback").text(message).css("color", color);
}

$("#btn-dice").click(rollDice);
