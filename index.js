const mongoose = require("mongoose");
const cors = require("cors");
const express = require("express");
const app = express();
const path = require("path");

app.use(express.json());

const port = 8000;

// View User Page
app.use(express.static(__dirname + "/views"));

app.use(cors());

app.get("/", (req, res) => {
  res.sendFile(path.join(__dirname + "/views/index.html"));
});

// Connect to mongoDb Local Database
const mongoUrl = `mongodb://127.0.0.1:27017/CRUD_Casino`;
mongoose.connect(mongoUrl).then(() => console.log(`DB Connection to Local Database Successfully!`));

// Router
const { diceRouter } = require("./app/routers/diceRouter");
const { userRouter } = require("./app/routers/userRouter");
const { diceHistoryRouter } = require("./app/routers/diceHistoryRouter");
const { prizeRouter } = require("./app/routers/prizeRouter");
const { voucherRouter } = require("./app/routers/voucherRouter");
const { voucherHistoryRouter } = require("./app/routers/voucherHistoryRouter");
const { prizeHistoryRouter } = require("./app/routers/prizeHistoryRouter");

app.use("/devcamp-lucky-dice", diceRouter);
app.use("/devcamp-lucky-dice", userRouter);
app.use("/devcamp-lucky-dice", diceHistoryRouter);
app.use("/devcamp-lucky-dice", prizeRouter);
app.use("/devcamp-lucky-dice", voucherRouter);
app.use("/devcamp-lucky-dice", voucherHistoryRouter);
app.use("/devcamp-lucky-dice", prizeHistoryRouter);

app.listen(port, () => console.log(`App is running on port ${port}`));
